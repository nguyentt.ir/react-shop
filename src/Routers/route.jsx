import React, { Component } from 'react';
import { BrowserRouter, Switch, Route } from 'react-router-dom';

import HomePage from 'pages/HomePage';
import ShopCategory from 'pages/Shop/ShopCategory';
import Header from 'components/sections/Header';
import Footer from 'components/sections/Footer';
import ContactUs from 'pages/ContactUs';
import Blogs from 'pages/Blogs/Blogs';
import SingleBlog from 'pages/Blogs/SingleBlog';
import Login from 'pages/Pages/Login';
import Tracking from 'pages/Pages/Tracking';
import Elements from 'pages/Pages/Elements';
import ShopSingleProduct from 'pages/Shop/ShopSingleProduct';
import ShopProductCheckout from 'pages/Shop/ShopProductCheckout';
import ShopCart from 'pages/Shop/ShopCart';
import ShopConfirmation from 'pages/Shop/ShopConfirmation';

const routes = [
    {
        path: "/",
        component: HomePage
    },
    {
        path: "/category",
        component: ShopCategory
    },
    {
        path: "/single-product",
        component: ShopSingleProduct
    },
    {
        path: "/checkout",
        component: ShopProductCheckout
    },
    {
        path: "/cart",
        component: ShopCart
    },
    {
        path: "/confirmation",
        component: ShopConfirmation
    },
    {
        path: "/blog",
        component: Blogs
    },
    {
        path: "/single-blog",
        component: SingleBlog
    },
    {
        path: "/login",
        component: Login
    },
    {
        path: "/tracking",
        component: Tracking
    },
    {
        path: "/elements",
        component: Elements
    },
    {
        path: "/contact",
        component: ContactUs
    }

]

class Layouts extends Component {
    render() {
        return (
            <BrowserRouter>
                <Header />
                <Switch>
                    {routes.map(route => <Route exact path={route.path} component={route.component} />)}
                </Switch>
                <Footer />
            </BrowserRouter>
        );
    }
}

export default Layouts;