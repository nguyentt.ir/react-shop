import React, { Component } from 'react';

class Category extends Component {
  render() {
    return (
      <div className={this.props.className}>
        <div className="single-deal">
          <div className="overlay" />
          <img className="img-fluid w-100" src={this.props.img} alt="" />
          <a
            href={this.props.img}
            className="img-pop-up"
            target="_blank"
            rel="noopener noreferrer"
          >
            <div className="deal-details">
              <h6 className="deal-title">{this.props.content}</h6>
            </div>
          </a>
        </div>
      </div>
    );
  }
}

export default Category;
