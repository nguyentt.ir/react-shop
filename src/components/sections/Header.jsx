import React, { Component } from "react";
import logo from "../../assets/img/logo.png";

class Header extends Component {
  myRef;
    constructor(props) {
        super(props);
        this.state = {
            isSticky: false
        }
        this.myRef = React.createRef();
    }
    
    componentDidMount() {
      document.addEventListener('scroll', this.handleScroll);
    }
    
    componentWillUnmount() {
      document.removeEventListener('scroll', this.handleScroll);
    }
    
    handleScroll = () => {
      var scrolHeaderToTop = this.myRef.current.getBoundingClientRect().top;
      
      if (scrolHeaderToTop <= 0) {
        this.setState({
          isSticky: true
        })
        
      }else {
        this.setState({
          isSticky: false
        })
      }
    };


  render() {
      const {isSticky} = this.state;
      
    return (
      <div
        className={`sticky-wrapper ${isSticky ? "is-sticky" : ""}`}
        ref={this.myRef}
      >
        <header className="header_area sticky-header">
          <div className="main_menu">
            <nav className="navbar navbar-expand-lg navbar-light main_box">
              <div className="container">
                <a className="navbar-brand logo_h" href="/">
                  <img src={logo} alt="" />
                </a>
                <button
                  className="navbar-toggler"
                  type="button"
                  data-toggle="collapse"
                  data-target="#navbarSupportedContent"
                  aria-controls="navbarSupportedContent"
                  aria-expanded="false"
                  aria-label="Toggle navigation"
                >
                  <span className="icon-bar" />
                  <span className="icon-bar" />
                  <span className="icon-bar" />
                </button>
                <div
                  className="collapse navbar-collapse offset"
                  id="navbarSupportedContent"
                >
                  <ul className="nav navbar-nav menu_nav ml-auto">
                    <li className="nav-item active">
                      <a className="nav-link" href="/">
                        Home
                      </a>
                    </li>
                    <li className="nav-item submenu dropdown">
                      <a
                        href="#"
                        className="nav-link dropdown-toggle"
                        data-toggle="dropdown"
                        role="button"
                        aria-haspopup="true"
                        aria-expanded="false"
                      >
                        Shop
                      </a>
                      <ul className="dropdown-menu">
                        <li className="nav-item">
                          <a className="nav-link" href="/category">
                            Shop Category
                          </a>
                        </li>
                        <li className="nav-item">
                          <a className="nav-link" href="/single-product">
                            Product Details
                          </a>
                        </li>
                        <li className="nav-item">
                          <a className="nav-link" href="/checkout">
                            Product Checkout
                          </a>
                        </li>
                        <li className="nav-item">
                          <a className="nav-link" href="/cart">
                            Shopping Cart
                          </a>
                        </li>
                        <li className="nav-item">
                          <a className="nav-link" href="/confirmation">
                            Confirmation
                          </a>
                        </li>
                      </ul>
                    </li>
                    <li className="nav-item submenu dropdown">
                      <a
                        href="/#"
                        className="nav-link dropdown-toggle"
                        data-toggle="dropdown"
                        role="button"
                        aria-haspopup="true"
                        aria-expanded="false"
                      >
                        Blog
                      </a>
                      <ul className="dropdown-menu">
                        <li className="nav-item">
                          <a className="nav-link" href="/blog">
                            Blog
                          </a>
                        </li>
                        <li className="nav-item">
                          <a className="nav-link" href="/single-blog">
                            Blog Details
                          </a>
                        </li>
                      </ul>
                    </li>
                    <li className="nav-item submenu dropdown">
                      <a
                        href="/#"
                        className="nav-link dropdown-toggle"
                        data-toggle="dropdown"
                        role="button"
                        aria-haspopup="true"
                        aria-expanded="false"
                      >
                        Pages
                      </a>
                      <ul className="dropdown-menu">
                        <li className="nav-item">
                          <a className="nav-link" href="/login">
                            Login
                          </a>
                        </li>
                        <li className="nav-item">
                          <a className="nav-link" href="/tracking">
                            Tracking
                          </a>
                        </li>
                        <li className="nav-item">
                          <a className="nav-link" href="/elements">
                            Elements
                          </a>
                        </li>
                      </ul>
                    </li>
                    <li className="nav-item">
                      <a className="nav-link" href="/contact">
                        Contact
                      </a>
                    </li>
                  </ul>
                  <ul className="nav navbar-nav navbar-right">
                    <li className="nav-item">
                      <a href="/" className="cart">
                        <span className="ti-bag" />
                      </a>
                    </li>
                    <li className="nav-item">
                      <button className="search">
                        <span className="lnr lnr-magnifier" id="search" />
                      </button>
                    </li>
                  </ul>
                </div>
              </div>
            </nav>
          </div>
          <div
            className="search_input"
            id="search_input_box"
            style={{ display: "none" }}
          >
            <div className="container">
              <form className="d-flex justify-content-between">
                <input
                  type="text"
                  className="form-control"
                  id="search_input"
                  placeholder="Search Here"
                />
                <button type="submit" className="btn" />
                <span
                  className="lnr lnr-cross"
                  id="close_search"
                  title="Close Search"
                />
              </form>
            </div>
          </div>
        </header>
      </div>
    );
  }
}

export default Header;
