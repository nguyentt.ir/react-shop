import React, { Component } from 'react';
import OwlCarousel from 'react-owl-carousel';

//Hard img
import prev from '../../assets/img/banner/prev.png';
import next from '../../assets/img/banner/next.png';
import bannerImg from '../../assets/img/banner/banner-img.png';

class IntroHome extends Component {
  render() {
    const options = {
      items: 1,
      autoplay: false,
      autoplayTimeout: 5000,
      loop: true,
      nav: true,
      navText: [`<img src='${prev}'>`, `<img src='${next}'>`],
      dots: false
    };
    return (
      <>
        <section className="banner-area">
          <div className="container">
            <div
              className="row fullscreen align-items-center justify-content-start"
              style={{ height: '700px' }}
            >
              <div className="col-lg-12">
                <OwlCarousel
                  className="active-banner-slider owl-carousel"
                  {...options}
                >
                  <div className="row single-slide align-items-center d-flex">
                    <div className="col-lg-5 col-md-6">
                      <div className="banner-content">
                        <h1>
                          Nike New <br />
                          Collection!
                        </h1>
                        <p>
                          Lorem ipsum dolor sit amet, consectetur adipisicing
                          elit, sed do eiusmod tempor incididunt ut labore et
                          dolore magna aliqua. Ut enim ad minim veniam, quis
                          nostrud exercitation.
                        </p>
                        <div className="add-bag d-flex align-items-center">
                          <a className="add-btn" href>
                            <span className="lnr lnr-cross" />
                          </a>
                          <span className="add-text text-uppercase">
                            Add to Bag
                          </span>
                        </div>
                      </div>
                    </div>
                    <div className="col-lg-7">
                      <div className="banner-img">
                        <img className="img-fluid" src={bannerImg} alt="" />
                      </div>
                    </div>
                  </div>
                  <div className="row single-slide">
                    <div className="col-lg-5">
                      <div className="banner-content">
                        <h1>
                          Nike New <br />
                          Collection!
                        </h1>
                        <p>
                          Lorem ipsum dolor sit amet, consectetur adipisicing
                          elit, sed do eiusmod tempor incididunt ut labore et
                          dolore magna aliqua. Ut enim ad minim veniam, quis
                          nostrud exercitation.
                        </p>
                        <div className="add-bag d-flex align-items-center">
                          <a className="add-btn" href>
                            <span className="lnr lnr-cross" />
                          </a>
                          <span className="add-text text-uppercase">
                            Add to Bag
                          </span>
                        </div>
                      </div>
                    </div>
                    <div className="col-lg-7">
                      <div className="banner-img">
                        <img className="img-fluid" src={bannerImg} alt="" />
                      </div>
                    </div>
                  </div>
                </OwlCarousel>
              </div>
            </div>
          </div>
        </section>
      </>
    );
  }
}

export default IntroHome;
