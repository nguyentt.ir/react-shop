import React from 'react';

const actionItem = [
  {
    icon: 'ti-bag',
    content: 'add to bag'
  },
  {
    icon: 'lnr lnr-heart',
    content: 'Wishlist'
  },
  {
    icon: 'lnr lnr-sync',
    content: 'compare'
  },
  {
    icon: 'lnr lnr-move',
    content: 'view more'
  }
];

const Product = (props) => {
  return (
    <div className="col-lg-3 col-md-6">
      <div className="single-product">
        <img className="img-fluid" src={props.img} alt="" />
        <div className="product-details">
          <h6>{props.title}</h6>
          <div className="price">
            <h6>{props.price}</h6>
            <h6 className="l-through">{props.oldPrice}</h6>
          </div>
          <div className="prd-bottom">
            {actionItem.map((item) => {
              return (
                <a href className="social-info">
                  <span className={item.icon} />
                  <p className="hover-text">{item.content}</p>
                </a>
              );
            })}
          </div>
        </div>
      </div>
    </div>
  );
};

export default Product;
