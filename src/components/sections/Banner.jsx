import React from 'react';

function Banner({ title, level2 = '', path, currentPageText }) {
  return (
    <section className="banner-area organic-breadcrumb">
      <div className="container">
        <div className="breadcrumb-banner d-flex flex-wrap align-items-center justify-content-end">
          <div className="col-first">
            <h1>{title}</h1>
            <nav className="d-flex align-items-center">
              <a href="/">
                Home
                <span className="lnr lnr-arrow-right" />
              </a>
              {level2}
              <a href={path}>{currentPageText}</a>
            </nav>
          </div>
        </div>
      </div>
    </section>
  );
}

export default Banner;
