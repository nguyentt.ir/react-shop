import React, { Component } from 'react';

import r1 from '../../assets/img/product/r1.jpg';
import r2 from '../../assets/img/product/r2.jpg';
import r3 from '../../assets/img/product/r3.jpg';
import r4 from '../../assets/img/product/r4.jpg';
import r5 from '../../assets/img/product/r5.jpg';
import r6 from '../../assets/img/product/r6.jpg';
import r7 from '../../assets/img/product/r7.jpg';
import r8 from '../../assets/img/product/r8.jpg';
import r9 from '../../assets/img/product/r9.jpg';
import r10 from '../../assets/img/product/r9.jpg';
import r11 from '../../assets/img/product/r9.jpg';

import c5 from '../../assets/img/category/c5.jpg';

const RelatedProDuctItem = ({ className, img, title, price, oldPrice }) => {
  return (
    <div className={className}>
      <div className="single-related-product d-flex">
        <a href="#">
          <img src={img} alt="" />
        </a>
        <div className="desc">
          <a href="#" className="title">
            {title}
          </a>
          <div className="price">
            <h6>{price}</h6>
            <h6 className="l-through">{oldPrice}</h6>
          </div>
        </div>
      </div>
    </div>
  );
};

const RelateProductList = [
  {
    className: 'col-lg-4 col-md-4 col-sm-6 mb-20',
    img: r1,
    title: 'Black lace Heels',
    price: '$189.00',
    oldPrice: '$210.00'
  },
  {
    className: 'col-lg-4 col-md-4 col-sm-6 mb-20',
    img: r2,
    title: 'Black lace Heels',
    price: '$189.00',
    oldPrice: '$210.00'
  },
  {
    className: 'col-lg-4 col-md-4 col-sm-6 mb-20',
    img: r3,
    title: 'Black lace Heels',
    price: '$189.00',
    oldPrice: '$210.00'
  },
  {
    className: 'col-lg-4 col-md-4 col-sm-6 mb-20',
    img: r4,
    title: 'Black lace Heels',
    price: '$189.00',
    oldPrice: '$210.00'
  },
  {
    className: 'col-lg-4 col-md-4 col-sm-6 mb-20',
    img: r5,
    title: 'Black lace Heels',
    price: '$189.00',
    oldPrice: '$210.00'
  },
  {
    className: 'col-lg-4 col-md-4 col-sm-6 mb-20',
    img: r6,
    title: 'Black lace Heels',
    price: '$189.00',
    oldPrice: '$210.00'
  },
  {
    className: 'col-lg-4 col-md-4 col-sm-6',
    img: r7,
    title: 'Black lace Heels',
    price: '$189.00',
    oldPrice: '$210.00'
  },
  {
    className: 'col-lg-4 col-md-4 col-sm-6',
    img: r8,
    title: 'Black lace Heels',
    price: '$189.00',
    oldPrice: '$210.00'
  },
  {
    className: 'col-lg-4 col-md-4 col-sm-6',
    img: r9,
    title: 'Black lace Heels',
    price: '$189.00',
    oldPrice: '$210.00'
  }
];

class RelateProducts extends Component {
  render() {
    return (
      <section className="related-product-area section_gap_bottom">
        <div className="container">
          <div className="row justify-content-center">
            <div className="col-lg-6 text-center">
              <div className="section-title">
                <h1>Deals of the Week</h1>
                <p>
                  Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed
                  do eiusmod tempor incididunt ut labore et dolore magna aliqua.
                </p>
              </div>
            </div>
          </div>
          <div className="row">
            <div className="col-lg-9">
              <div className="row">
                {RelateProductList.map((item) => {
                  return (
                    <RelatedProDuctItem
                      className={item.className}
                      img={item.img}
                      title={item.title}
                      price={item.price}
                      oldPrice={item.oldPrice}
                    />
                  );
                })}
              </div>
            </div>
            <div className="col-lg-3">
              <div className="ctg-right">
                <a href="#" target="_blank">
                  <img className="img-fluid d-block mx-auto" src={c5} alt="" />
                </a>
              </div>
            </div>
          </div>
        </div>
      </section>
    );
  }
}

export default RelateProducts;
