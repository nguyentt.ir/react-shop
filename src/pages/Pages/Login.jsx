import React, { Component } from 'react';

import login from '../../assets/img/pages/login.jpg';
import Banner from 'components/sections/Banner';

const loginBoxAreaSection = () => {
  return (
    <section className="login_box_area section_gap">
      <div className="container">
        <div className="row">
          <div className="col-lg-6">
            <div className="login_box_img">
              <img className="img-fluid" src={login} alt="" />
              <div className="hover">
                <h4>New to our website?</h4>
                <p>
                  There are advances being made in science and technology
                  everyday, and a good example of this is the
                </p>
                <a className="primary-btn" href="registration.html">
                  Create an Account
                </a>
              </div>
            </div>
          </div>
          <div className="col-lg-6">
            <div className="login_form_inner">
              <h3>Log in to enter</h3>
              <form
                className="row login_form"
                action="contact_process.php"
                method="post"
                id="contactForm"
                noValidate="novalidate"
              >
                <div className="col-md-12 form-group">
                  <input
                    type="text"
                    className="form-control"
                    id="name"
                    name="name"
                    placeholder="Username"
                    onfocus="if (!window.__cfRLUnblockHandlers) return false; this.placeholder = ''"
                    onblur="if (!window.__cfRLUnblockHandlers) return false; this.placeholder = 'Username'"
                    data-cf-modified-bb49a8d6870e7b38baf6c3b7-
                  />
                </div>
                <div className="col-md-12 form-group">
                  <input
                    type="text"
                    className="form-control"
                    id="name"
                    name="name"
                    placeholder="Password"
                    onfocus="if (!window.__cfRLUnblockHandlers) return false; this.placeholder = ''"
                    onblur="if (!window.__cfRLUnblockHandlers) return false; this.placeholder = 'Password'"
                    data-cf-modified-bb49a8d6870e7b38baf6c3b7-
                  />
                </div>
                <div className="col-md-12 form-group">
                  <div className="creat_account">
                    <input type="checkbox" id="f-option2" name="selector" />
                    <label htmlFor="f-option2">Keep me logged in</label>
                  </div>
                </div>
                <div className="col-md-12 form-group">
                  <button type="submit" value="submit" className="primary-btn">
                    Log In
                  </button>
                  <a href="#">Forgot Password?</a>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
    </section>
  );
};

class Login extends Component {
  render() {
    return (
      <div className="pages-page">
        <Banner
          title="Login/Register"
          path="/login"
          currentPageText="Login/Register"
        />
        {loginBoxAreaSection()}
      </div>
    );
  }
}

export default Login;
