import React, { Component } from 'react';
import Banner from 'components/sections/Banner';

class Elements extends Component {
  render() {
    return (
      <div>
        <Banner
          title="Element Page"
          path="/element"
          currentPageText="Element"
        />
        <h1 style={{ textAlign: 'center', padding: '40px', color: '#ffba00' }}>
          Comming Soon...
        </h1>
      </div>
    );
  }
}

export default Elements;
