import React, { Component } from 'react';
import Banner from 'components/sections/Banner';

const trackingAreaSection = () => {
  return (
    <section className="tracking_box_area section_gap">
      <div className="container">
        <div className="tracking_box_inner">
          <p>
            To track your order please enter your Order ID in the box below and
            press the "Track" button. This was given to you on your receipt and
            in the confirmation email you should have received.
          </p>
          <form
            className="row tracking_form"
            action="#"
            method="post"
            noValidate="novalidate"
          >
            <div className="col-md-12 form-group">
              <input
                type="text"
                className="form-control"
                id="order"
                name="order"
                placeholder="Order ID"
                onfocus="if (!window.__cfRLUnblockHandlers) return false; this.placeholder = ''"
                onblur="if (!window.__cfRLUnblockHandlers) return false; this.placeholder = 'Order ID'"
                data-cf-modified-2197c1ebf6ed2ac226827fa3-
              />
            </div>
            <div className="col-md-12 form-group">
              <input
                type="email"
                className="form-control"
                id="email"
                name="email"
                placeholder="Billing Email Address"
                onfocus="if (!window.__cfRLUnblockHandlers) return false; this.placeholder = ''"
                onblur="if (!window.__cfRLUnblockHandlers) return false; this.placeholder = 'Billing Email Address'"
                data-cf-modified-2197c1ebf6ed2ac226827fa3-
              />
            </div>
            <div className="col-md-12 form-group">
              <button type="submit" value="submit" className="primary-btn">
                Track Order
              </button>
            </div>
          </form>
        </div>
      </div>
    </section>
  );
};

class Tracking extends Component {
  render() {
    return (
      <div>
        <Banner
          title="Order Tracking"
          path="/tracking"
          currentPageText="Order Tracking"
        />
        {trackingAreaSection()}
      </div>
    );
  }
}

export default Tracking;
