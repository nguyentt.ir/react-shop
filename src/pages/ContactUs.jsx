import React, { Component } from 'react';
import Banner from 'components/sections/Banner';

const contactAreaSection = () => {
  return (
    <section className="contact_area section_gap_bottom">
      <div className="container">
        <div id="mapBox" className="mapBox">
          <iframe
            src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3919.117611384853!2d106.63846331480096!3d10.802303292303973!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3175294f46ce189d%3A0x875b87cf82f8476d!2zMzY0IEPhu5luZyBIw7JhLCBQaMaw4budbmcgMTMsIFTDom4gQsOsbmgsIEjhu5MgQ2jDrSBNaW5o!5e0!3m2!1svi!2s!4v1589509791566!5m2!1svi!2s"
            height={450}
            frameBorder={0}
            style={({ border: 0 }, { width: '100%' })}
            allowFullScreen
            aria-hidden="false"
            tabIndex={0}
          />
        </div>
        <div className="row">
          <div className="col-lg-3">
            <div className="contact_info">
              <div className="info_item">
                <i className="lnr lnr-home" />
                <h6>Ho Chi Minh, Vietnam</h6>
                <p>Santa monica bullevard</p>
              </div>
              <div className="info_item">
                <i className="lnr lnr-phone-handset" />
                <h6>
                  <a href="#">84 (440) 9865 562</a>
                </h6>
                <p>Mon to Fri 9am to 6 pm</p>
              </div>
              <div className="info_item">
                <i className="lnr lnr-envelope" />
                <h6>
                  <a href="#">
                    <span className="__cf_email__">nguyentt.ir@gmail.com</span>
                  </a>
                </h6>
                <p>Send us your query anytime!</p>
              </div>
            </div>
          </div>
          <div className="col-lg-9">
            <form
              className="row contact_form"
              method="post"
              id="contactForm"
              noValidate="novalidate"
            >
              <div className="col-md-6">
                <div className="form-group">
                  <input
                    type="text"
                    className="form-control"
                    id="name"
                    name="name"
                    placeholder="Enter your name"
                    onfocus="if (!window.__cfRLUnblockHandlers) return false; this.placeholder = ''"
                    onblur="if (!window.__cfRLUnblockHandlers) return false; this.placeholder = 'Enter your name'"
                    data-cf-modified-87542db58fed2aafad8ef6eb-
                  />
                </div>
                <div className="form-group">
                  <input
                    type="email"
                    className="form-control"
                    id="email"
                    name="email"
                    placeholder="Enter email address"
                    onfocus="if (!window.__cfRLUnblockHandlers) return false; this.placeholder = ''"
                    onblur="if (!window.__cfRLUnblockHandlers) return false; this.placeholder = 'Enter email address'"
                    data-cf-modified-87542db58fed2aafad8ef6eb-
                  />
                </div>
                <div className="form-group">
                  <input
                    type="text"
                    className="form-control"
                    id="subject"
                    name="subject"
                    placeholder="Enter Subject"
                    onfocus="if (!window.__cfRLUnblockHandlers) return false; this.placeholder = ''"
                    onblur="if (!window.__cfRLUnblockHandlers) return false; this.placeholder = 'Enter Subject'"
                  />
                </div>
              </div>
              <div className="col-md-6">
                <div className="form-group">
                  <textarea
                    className="form-control"
                    name="message"
                    id="message"
                    rows={1}
                    placeholder="Enter Message"
                    onfocus="if (!window.__cfRLUnblockHandlers) return false; this.placeholder = ''"
                    onblur="if (!window.__cfRLUnblockHandlers) return false; this.placeholder = 'Enter Message'"
                    data-cf-modified-87542db58fed2aafad8ef6eb-
                    defaultValue={''}
                  />
                </div>
              </div>
              <div className="col-md-12 text-right">
                <button type="submit" value="submit" className="primary-btn">
                  Send Message
                </button>
              </div>
            </form>
          </div>
        </div>
      </div>
    </section>
  );
};

class ContactUs extends Component {
  render() {
    return (
      <div className="contact-page">
        <Banner title="Contact Us" path="/contact" currentPageText="Contact" />
        {contactAreaSection()}
      </div>
    );
  }
}

export default ContactUs;
