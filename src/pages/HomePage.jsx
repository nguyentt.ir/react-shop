import React, { Component } from 'react';
import OwlCarousel from 'react-owl-carousel';
import IntroHome from 'components/sections/IntroHome';
import RelateProducts from 'components/sections/RelateProducts';

// Hard img
import fIcon1 from '../assets/img/features/f-icon1.png';
import fIcon2 from '../assets/img/features/f-icon2.png';
import fIcon3 from '../assets/img/features/f-icon3.png';
import fIcon4 from '../assets/img/features/f-icon4.png';

import c1 from '../assets/img/category/c1.jpg';
import c2 from '../assets/img/category/c2.jpg';
import c3 from '../assets/img/category/c3.jpg';
import c4 from '../assets/img/category/c4.jpg';
import c5 from '../assets/img/category/c5.jpg';

import p1 from '../assets/img/product/p1.jpg';
import p2 from '../assets/img/product/p2.jpg';
import p3 from '../assets/img/product/p3.jpg';
import p4 from '../assets/img/product/p4.jpg';
import p5 from '../assets/img/product/p5.jpg';
import p6 from '../assets/img/product/p6.jpg';
import p7 from '../assets/img/product/p7.jpg';
import p8 from '../assets/img/product/p8.jpg';
import e1 from '../assets/img/product/e-p1.png';

import b1 from '../assets/img/brand/1.png';
import b2 from '../assets/img/brand/2.png';
import b3 from '../assets/img/brand/3.png';
import b4 from '../assets/img/brand/4.png';
import b5 from '../assets/img/brand/5.png';

//Hard img carousel
import prev from '../assets/img/product/prev.png';
import next from '../assets/img/product/next.png';
import Category from 'components/sections/Category';
import Product from 'components/sections/Product';

const featureAreaSection = () => {
  const featureData = [
    {
      image: fIcon1,
      feaTitle: 'Free Delivery',
      feaDesc: 'Free Shipping on all order'
    },
    {
      image: fIcon2,
      feaTitle: 'Return Policy',
      feaDesc: 'Free Shipping on all order'
    },
    {
      image: fIcon3,
      feaTitle: '24/7 Support',
      feaDesc: 'Free Shipping on all order'
    },
    {
      image: fIcon4,
      feaTitle: 'Secure Payment',
      feaDesc: 'Free Shipping on all order'
    }
  ];

  return (
    <section className="features-area section_gap">
      <div className="container">
        <div className="row features-inner">
          {featureData.map((item) => (
            <div className="col-lg-3 col-md-6 col-sm-6">
              <div className="single-features">
                <div className="f-icon">
                  <img src={item.image} alt="" />
                </div>
                <h6>{item.feaTitle}</h6>
                <p>{item.feaDesc}</p>
              </div>
            </div>
          ))}
        </div>
      </div>
    </section>
  );
};

const categoryAreaSection = () => {
  return (
    <section className="category-area">
      <div className="container">
        <div className="row justify-content-center">
          <div className="col-lg-8 col-md-12">
            <div className="row">
              <Category
                className="col-lg-8 col-md-8"
                img={c1}
                content="Sneaker for Sports"
              />
              <Category
                className="col-lg-4 col-md-4"
                img={c2}
                content="Sneaker for Sports"
              />
              <Category
                className="col-lg-4 col-md-4"
                img={c3}
                content="Sneaker for Sports"
              />
              <Category
                className="col-lg-8 col-md-8"
                img={c4}
                content="Sneaker for Sports"
              />
            </div>
          </div>
          <Category
            className="col-lg-4 col-md-6"
            img={c5}
            content="Sneaker for Sports"
          />
        </div>
      </div>
    </section>
  );
};

const productAreaSection = () => {
  const options = {
    items: 1,
    autoplay: false,
    autoplayTimeout: 5000,
    loop: true,
    nav: true,
    navText: [`<img src='${prev}'>`, `<img src='${next}'>`],
    dots: false
  };

  const latesProducts = [
    {
      img: p1,
      title: 'addidas New Hammer sole for Sports person',
      price: '$150.00',
      oldPrice: '$210.00'
    },
    {
      img: p2,
      title: 'addidas New Hammer sole for Sports person',
      price: '$150.00',
      oldPrice: '$210.00'
    },
    {
      img: p3,
      title: 'addidas New Hammer sole for Sports person',
      price: '$150.00',
      oldPrice: '$210.00'
    },
    {
      img: p4,
      title: 'addidas New Hammer sole for Sports person',
      price: '$150.00',
      oldPrice: '$210.00'
    },
    {
      img: p5,
      title: 'addidas New Hammer sole for Sports person',
      price: '$150.00',
      oldPrice: '$210.00'
    },
    {
      img: p6,
      title: 'addidas New Hammer sole for Sports person',
      price: '$150.00',
      oldPrice: '$210.00'
    },
    {
      img: p7,
      title: 'addidas New Hammer sole for Sports person',
      price: '$150.00',
      oldPrice: '$210.00'
    },
    {
      img: p8,
      title: 'addidas New Hammer sole for Sports person',
      price: '$150.00',
      oldPrice: '$210.00'
    }
  ];

  const commingProducts = [
    {
      img: p8,
      title: 'addidas New Hammer sole for Sports person',
      price: '$150.00',
      oldPrice: '$210.00'
    },
    {
      img: p7,
      title: 'addidas New Hammer sole for Sports person',
      price: '$150.00',
      oldPrice: '$210.00'
    },
    {
      img: p6,
      title: 'addidas New Hammer sole for Sports person',
      price: '$150.00',
      oldPrice: '$210.00'
    },
    {
      img: p5,
      title: 'addidas New Hammer sole for Sports person',
      price: '$150.00',
      oldPrice: '$210.00'
    },
    {
      img: p4,
      title: 'addidas New Hammer sole for Sports person',
      price: '$150.00',
      oldPrice: '$210.00'
    },
    {
      img: p3,
      title: 'addidas New Hammer sole for Sports person',
      price: '$150.00',
      oldPrice: '$210.00'
    },
    {
      img: p2,
      title: 'addidas New Hammer sole for Sports person',
      price: '$150.00',
      oldPrice: '$210.00'
    },
    {
      img: p1,
      title: 'addidas New Hammer sole for Sports person',
      price: '$150.00',
      oldPrice: '$210.00'
    }
  ];
  return (
    <OwlCarousel
      className="owl-carousel active-product-area section_gap"
      {...options}
    >
      <div className="single-product-slider">
        <div className="container">
          <div className="row justify-content-center">
            <div className="col-lg-6 text-center">
              <div className="section-title">
                <h1>Latest Products</h1>
                <p>
                  Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed
                  do eiusmod tempor incididunt ut labore et dolore magna aliqua.
                </p>
              </div>
            </div>
          </div>
          <div className="row">
            {latesProducts.map((item) => {
              return (
                <Product
                  img={item.img}
                  title={item.title}
                  price={item.price}
                  oldPrice={item.oldPrice}
                />
              );
            })}
          </div>
        </div>
      </div>
      <div className="single-product-slider">
        <div className="container">
          <div className="row justify-content-center">
            <div className="col-lg-6 text-center">
              <div className="section-title">
                <h1>Coming Products</h1>
                <p>
                  Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed
                  do eiusmod tempor incididunt ut labore et dolore magna aliqua.
                </p>
              </div>
            </div>
          </div>
          <div className="row">
            {commingProducts.map((item) => {
              return (
                <Product
                  img={item.img}
                  title={item.title}
                  price={item.price}
                  oldPrice={item.oldPrice}
                />
              );
            })}
          </div>
        </div>
      </div>
    </OwlCarousel>
  );
};

const exclusiveDealAreaSection = () => {
  const options = {
    items: 1,
    autoplay: false,
    autoplayTimeout: 5000,
    loop: true,
    nav: true,
    navText: [`<img src='${prev}'>`, `<img src='${next}'>`],
    dots: false
  };

  const singleExclusiveSliderItem = [
    {
      img: e1,
      price: '$150.00',
      oldPrice: '$210.00',
      title: 'addidas New Hammer sole for Sports person'
    },
    {
      img: e1,
      price: '$150.00',
      oldPrice: '$210.00',
      title: 'addidas New Hammer sole for Sports person'
    }
  ];

  return (
    <section className="exclusive-deal-area">
      <div className="container-fluid">
        <div className="row justify-content-center align-items-center">
          <div className="col-lg-6 no-padding exclusive-left">
            <div className="row clock_sec clockdiv" id="clockdiv">
              <div className="col-lg-12">
                <h1>Exclusive Hot Deal Ends Soon!</h1>
                <p>Who are in extremely love with eco friendly system.</p>
              </div>
              <div className="col-lg-12">
                <div className="row clock-wrap">
                  <div className="col clockinner1 clockinner">
                    <h1 className="days">150</h1>
                    <span className="smalltext">Days</span>
                  </div>
                  <div className="col clockinner clockinner1">
                    <h1 className="hours">23</h1>
                    <span className="smalltext">Hours</span>
                  </div>
                  <div className="col clockinner clockinner1">
                    <h1 className="minutes">47</h1>
                    <span className="smalltext">Mins</span>
                  </div>
                  <div className="col clockinner clockinner1">
                    <h1 className="seconds">59</h1>
                    <span className="smalltext">Secs</span>
                  </div>
                </div>
              </div>
            </div>
            <a href className="primary-btn">
              Shop Now
            </a>
          </div>
          <div className="col-lg-6 no-padding exclusive-right">
            <OwlCarousel
              className="active-exclusive-product-slider"
              {...options}
            >
              {singleExclusiveSliderItem.map((item) => {
                return (
                  <div className="single-exclusive-slider">
                    <img className="img-fluid" src={e1} alt="" />
                    <div className="product-details">
                      <div className="price">
                        <h6>$150.00</h6>
                        <h6 className="l-through">$210.00</h6>
                      </div>
                      <h4>addidas New Hammer sole for Sports person</h4>
                      <div className="add-bag d-flex align-items-center justify-content-center">
                        <a className="add-btn" href>
                          <span className="ti-bag" />
                        </a>
                        <span className="add-text text-uppercase">
                          Add to Bag
                        </span>
                      </div>
                    </div>
                  </div>
                );
              })}
            </OwlCarousel>
          </div>
        </div>
      </div>
    </section>
  );
};

const brandAreaSection = () => {
  const brands = [
    { img: b1 },
    { img: b2 },
    { img: b3 },
    { img: b4 },
    { img: b5 }
  ];
  return (
    <section className="brand-area section_gap">
      <div className="container">
        <div className="row">
          {brands.map((item) => {
            return (
              <a className="col single-img" href="#">
                <img
                  className="img-fluid d-block mx-auto"
                  src={item.img}
                  alt=""
                />
              </a>
            );
          })}
        </div>
      </div>
    </section>
  );
};

class HomePage extends Component {
  render() {
    return (
      <>
        <IntroHome />
        {featureAreaSection()}
        {categoryAreaSection()}
        {productAreaSection()}
        {exclusiveDealAreaSection()}
        {brandAreaSection()}
        <RelateProducts />
      </>
    );
  }
}

export default HomePage;
