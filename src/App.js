import React from 'react';
import Layouts from 'Routers/route';
import store from 'Redux/store';
import {Provider} from 'react-redux';

function App() {
  return (
    <Provider store={store}>
      <Layouts />
    </Provider>
  );
}

export default App;
